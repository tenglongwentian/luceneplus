package com.ld.zxw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.alibaba.fastjson.JSONArray;
import com.ld.zxw.Analyzer.AnalyzerType;
import com.ld.zxw.config.Config;
import com.ld.zxw.config.LuceneService;
import com.ld.zxw.config.LuceneServiceImpl;
import com.ld.zxw.page.Page;
import com.ld.zxw.plugin.LucenePlugin;
@SuppressWarnings("all")
public class TestLucenePlus {
	private static Logger log = Logger.getLogger(TestLucenePlus.class);

	private static LuceneService service = new LuceneServiceImpl();

	private static Config config;

	//预加载配置
	static{
		LdConfig ldconfig = new LdConfig();
		config = ldconfig.start();
	}

	public static void main(String[] args) throws Exception {
		LucenePlugin lucenePlugin = new LucenePlugin(config, "test");

		//添加
		//service.saveObjs(getDate());
		
		//删除
		service.delAll();
		
		//查询 全部
		//JSONArray queryList = service.queryList("百度");
		//System.out.println(queryList.size());
		
		//分页查询
		//Page<User> queryList = service.queryList("百度", 0*10, 10, new User());
		//System.out.println(queryList.getPageSize());
		
		//更新
		//service.updateObj(new User());
	}


	public static List<Object> getDate(){
		List<Object> list = new ArrayList<>();
		for (int i = 1; i < 100; i++) {
			User user = new User();
			user.setId(i);
			user.setName(null);
			user.setContent("被忽略，因为百度的查询限制在38个汉字以内");
			user.setSort(i);
			list.add(user);
		}
		return list;
	}

}
