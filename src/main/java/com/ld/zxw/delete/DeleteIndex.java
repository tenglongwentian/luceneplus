package com.ld.zxw.delete;

import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;

import com.ld.zxw.config.Config;
import com.ld.zxw.util.CommonUtil;

public class DeleteIndex {

	private Logger log = Logger.getLogger(DeleteIndex.class);

	public static Config config  = null;

	public boolean deleteAllIndex(){
		boolean flag =true;
		IndexWriter indexWriter = null;
		try {
			indexWriter = CommonUtil.getIndexWriter(config);
			indexWriter.deleteAll();
			indexWriter.commit();
		}catch (Exception e) {
			flag = false;
			log.error("清空索引-error:",e);
		}finally {
			CommonUtil.colseIndexWriter(indexWriter);
		}
		return flag;
	}
}
