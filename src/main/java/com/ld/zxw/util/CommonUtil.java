package com.ld.zxw.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;

import com.ld.zxw.Documents.Documents;
import com.ld.zxw.config.Config;

public class CommonUtil {

	private static Logger log = Logger.getLogger(CommonUtil.class);

	public synchronized static IndexWriter getIndexWriter(Config config) throws IOException{
		IndexWriterConfig conf = new IndexWriterConfig(config.getAnalyzer());
		return new IndexWriter(config.getDirectory(), conf);
	}

	public synchronized static void colseIndexWriter(IndexWriter writer){
		if(writer != null){
			try {
				writer.close();
			} catch (IOException e) {
				log.error("关闭流异常:",e);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public synchronized static List<Document> mapOrdoc(List<HashMap> list){
		List<Document> lists = new ArrayList<>();
		for ( HashMap hashMap : list) {
			Documents doc = new Documents();
			Iterator iterator = hashMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				doc.put(entry.getKey(), entry.getValue());
			}
			lists.add(doc.getDocument());
		}
		return lists;
	}
	@SuppressWarnings("rawtypes")
	public synchronized static Document mapOrdoc(HashMap map){
		Documents doc = new Documents();
		Iterator iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			doc.put(entry.getKey(), entry.getValue());
		}
		return doc.getDocument();
	}



	/**
	 * 过滤非法字符
	 * @param str 元数据
	 * @param filter 非法数据
	 * @return
	 */
	public synchronized static String IllegalFiltering(String str,String filter){
		if(StringUtils.isNotEmpty(filter)){
			return str;
		}else{
			char[] data = getChar(str);
			char[] fiter = getChar(filter);
			String key="";
			for (int i = 0; i < data.length; i++) {
				if(isFlag(fiter, data[i])){
					key+=String.valueOf(data[i]);
				}
			}
			return key;
		}
	}


	private static char[] getChar(String str){
		char[] c=new char[str.length()];
		c=str.toCharArray();
		return c;
	}

	public static boolean isFlag(char[] fiter,char vue){
		boolean flag = true;
		for (int j = 0; j < fiter.length; j++) {
			if(fiter[j] == vue){
				flag=false;
				break;
			}
		}
		return flag;
	}

}
