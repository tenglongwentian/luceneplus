package com.ld.zxw.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@SuppressWarnings("all")
public class ObjectUtil {
	
	
	public static HashMap objectToMap(Object obj) throws Exception {    
        if(obj == null)  
            return null;      
  
        HashMap map = new HashMap();   
  
        BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());    
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();    
        for (PropertyDescriptor property : propertyDescriptors) {    
            String key = property.getName();    
            if (key.compareToIgnoreCase("class") == 0) {   
                continue;  
            }  
            Method getter = property.getReadMethod();  
            Object value = getter!=null ? getter.invoke(obj) : null;  
            map.put(key, value);  
        }    
  
        return map;  
    }    
	
	public static List<HashMap> getListMap(List<Object> listk){
		List<HashMap> list = new ArrayList<>();
		for (Object object : listk) {
			try {
				list.add(objectToMap(object));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

}
