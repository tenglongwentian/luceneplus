package com.ld.zxw.plugin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;

import com.alibaba.fastjson.JSONArray;
import com.ld.zxw.Analyzer.AnalyzerType;
import com.ld.zxw.add.AddIndex;
import com.ld.zxw.config.Config;
import com.ld.zxw.delete.DeleteIndex;
import com.ld.zxw.init.Init;
import com.ld.zxw.page.Page;
import com.ld.zxw.query.QueryIndex;
import com.ld.zxw.update.UpdateIndex;
import com.ld.zxw.util.ObjectUtil;

@SuppressWarnings("all")
public class LucenePlugin {


	public LucenePlugin(Config config,String core,String AnalyzerType) throws IOException{
		//加载
		Init init = new Init(config, core, AnalyzerType);
		init.start();
	}
	public LucenePlugin(Config config,String core) throws IOException{
		//加载
		Init init = new Init(config, core, AnalyzerType.IKAnalyzer);
		init.start();
	}



}
