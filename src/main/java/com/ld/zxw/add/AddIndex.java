package com.ld.zxw.add;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;

import com.ld.zxw.Documents.Documents;
import com.ld.zxw.config.Config;
import com.ld.zxw.config.LdIndexWriter;
import com.ld.zxw.config.SaveIndexWriter;
import com.ld.zxw.util.CommonUtil;
@SuppressWarnings("all")
public class AddIndex {

	public static Config config  = null;

	private Logger log = Logger.getLogger(AddIndex.class);
	
	private LdIndexWriter ldIndexWriter = null;

	/**
	 * 添加索引
	 * @param list
	 * @throws IOException
	 */
	public boolean Index(List<HashMap> list){
		boolean flag = true;
		try {
			//添加索引
			ldIndexWriter = new SaveIndexWriter(config, CommonUtil.mapOrdoc(list));
			ldIndexWriter.commit();
		} catch (Exception e) {
			flag = false;
			log.error("添加数据-error",e);
		}
		return flag;
	}

}
