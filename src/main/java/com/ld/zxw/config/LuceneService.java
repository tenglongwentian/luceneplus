package com.ld.zxw.config;

import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.ld.zxw.page.Page;

/**
 * lucene 操作接口
 * @author Administrator
 *
 */
@SuppressWarnings("all")
public interface LuceneService {
	
	/**
	 * 添加对象集合
	 * @param objs
	 * @return
	 */
	boolean saveObjs(List<Object> objs);
	
	/**
	 * 添加map集合
	 * @param maps
	 * @return
	 */
	boolean saveMaps(List<HashMap> maps);
	
	/**
	 * 清空索引
	 * @return
	 */
	boolean delAll();
	
	
	/**
	 * 根据查询对象删除索引
	 * @param key
	 * @param value
	 * @return
	 */
	boolean delObj(String key,String value);
	
	
	/**
	 * 检索集合
	 * @param value
	 * @return
	 */
	JSONArray queryList(String value);
	
	
	/**
	 * 分页检索集合
	 * @param value 值
	 * @param pageNumber 当前页
	 * @param pageSize 每页条数
	 * @param obj 返回对象
	 * @return
	 */
	<T> Page<T> queryList(String value,int pageNumber,int pageSize,T obj);
	
	
	
	/**
	 * 更新对象集合
	 * @param objs
	 * @return
	 */
	boolean updateObjs(List<Object> objs);
	
	/**
	 * 更新map集合
	 * @param maps
	 * @return
	 */
	boolean updateMaps(List<HashMap> maps);
	
	
	/**
	 * 更新对象
	 * @param objs
	 * @return
	 */
	boolean updateObj(Object obj);
	
	/**
	 * 更新map
	 * @param maps
	 * @return
	 */
	boolean updateMap(HashMap map);

}
