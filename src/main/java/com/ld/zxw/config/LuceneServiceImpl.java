package com.ld.zxw.config;

import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.ld.zxw.add.AddIndex;
import com.ld.zxw.delete.DeleteIndex;
import com.ld.zxw.page.Page;
import com.ld.zxw.query.QueryIndex;
import com.ld.zxw.update.UpdateIndex;
import com.ld.zxw.util.ObjectUtil;

@SuppressWarnings("all")
public class LuceneServiceImpl implements LuceneService {
	
	private AddIndex addDao = new AddIndex();
	
	private DeleteIndex delDao = new DeleteIndex();
	
	private QueryIndex queryDao = new QueryIndex();
	
	private UpdateIndex updateDao = new UpdateIndex();

	/**
	 * 添加对象集合
	 */
	@Override
	public boolean saveObjs(List<Object> objs) {
		return addDao.Index(ObjectUtil.getListMap(objs));
	}

	/**
	 * 添加map集合
	 * @param maps
	 * @return
	 */
	@Override
	public boolean saveMaps(List<HashMap> maps) {
		return addDao.Index(maps);
	}

	/**
	 * 清空索引
	 */
	@Override
	public boolean delAll() {
		return delDao.deleteAllIndex();
	}

	@Override
	public boolean delObj(String key, String value) {
		return false;
	}

	@Override
	public JSONArray queryList(String value) {
		return queryDao.Index(value);
	}

	@Override
	public <T> Page<T> queryList(String value, int pageNumber, int pageSize, T obj) {
		return queryDao.Index(value, pageNumber, pageSize, obj);
	}

	@Override
	public boolean updateObjs(List<Object> objs) {
		return updateDao.Index(ObjectUtil.getListMap(objs));
	}

	@Override
	public boolean updateMaps(List<HashMap> maps) {
		return updateDao.Index(maps);
	}

	@Override
	public boolean updateObj(Object obj) {
		try {
			return updateDao.Index(ObjectUtil.objectToMap(obj));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateMap(HashMap map) {
		return updateMap(map);
	}
}
