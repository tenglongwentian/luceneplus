package com.ld.zxw.config;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;

import com.ld.zxw.util.CommonUtil;

public class SaveIndexWriter extends LdIndexWriter{
	
	public SaveIndexWriter(Config config, List<Document> document) {
		super(config, document);
	}

	@Override
	public void commit() throws IOException {
		IndexWriter indexWriter = CommonUtil.getIndexWriter(this.config);
		for (Document document : this.document) {
			indexWriter.addDocument(document);
		}
		indexWriter.commit();
		CommonUtil.colseIndexWriter(indexWriter);
	}
	

}
