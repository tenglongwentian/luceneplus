package com.ld.zxw.config;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;

import com.ld.zxw.util.CommonUtil;

public class UpdateIndexWriter extends LdIndexWriter{

	public UpdateIndexWriter(Config config, List<Document> document) {
		super(config, document);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void commit() throws Exception {
		IndexWriter indexWriter = CommonUtil.getIndexWriter(config);
		for (Document document : this.document) {
			String id = document.get("id");
			indexWriter.updateDocument(new Term("id",id), document);
		}
		indexWriter.commit();
		CommonUtil.colseIndexWriter(indexWriter);
	}

}
