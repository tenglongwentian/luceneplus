package com.ld.zxw.config;

import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.ControlledRealTimeReopenThread;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ReferenceManager;
import org.apache.lucene.store.FSDirectory;
@SuppressWarnings("all")
public class Config{
	//分词器选择
	
	private Analyzer analyzer = null;

	//高亮显示
	
	private String[]  highlight_conf = new String[]{"<font color='red'>","</font>"};
	
	//	索引根目录
	
	private String  lucene_path = null;

	//	非法字符
	private String  illegal_filtering = null;
	
	//索引目录
	private FSDirectory  directory = null;
	
	//查询
	private List<Map<String, String>>  isQuery = null;
	
	//全部字段
	private List<Map<String, String>>  xmlDem = null;
	
	
	/**nrt init**/
//    private TrackingIndexWriter trackingIndexWriter = null;
    private ReferenceManager<IndexSearcher> reMgr = null;//类似于Lucene3.x中的NrtManager
    private ControlledRealTimeReopenThread<IndexSearcher> crt = null;
	
	
	private int queryNum = 10;
	

	public int getQueryNum() {
		return queryNum;
	}

	public void setQueryNum(int queryNum) {
		this.queryNum = queryNum;
	}

	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}


	public String[] getHighlight_conf() {
		return highlight_conf;
	}

	public void setHighlight_conf(String[] highlight_conf) {
		this.highlight_conf = highlight_conf;
	}

	public String getLucene_path() {
		return lucene_path;
	}

	public void setLucene_path(String lucene_path) {
		this.lucene_path = lucene_path;
	}

	public String getIllegal_filtering() {
		return illegal_filtering;
	}

	public void setIllegal_filtering(String illegal_filtering) {
		this.illegal_filtering = illegal_filtering;
	}

	public FSDirectory getDirectory() {
		return directory;
	}

	public void setDirectory(FSDirectory directory) {
		this.directory = directory;
	}

	public List<Map<String, String>> getIsQuery() {
		return isQuery;
	}

	public void setIsQuery(List<Map<String, String>> isQuery) {
		this.isQuery = isQuery;
	}

	public List<Map<String, String>> getXmlDem() {
		return xmlDem;
	}

	public void setXmlDem(List<Map<String, String>> xmlDem) {
		this.xmlDem = xmlDem;
	}

}
